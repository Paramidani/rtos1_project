/*
 * i2c_slave.c
 *
 *  Created on: Apr 24, 2019
 *      Author: mparamidani
 */

#include "i2c_slave.h"


bool_t i2cSlaveInit( i2cMap_t i2cNumber, I2C_XFER_T *xfer, uint8_t addrMask )
{
	I2C_SLAVE_ID sid = I2C_SLAVE_0;//I2C_SLAVE_GENERAL;
	uint32_t clockRateHz = I2C_CLK_RATE;

	if(i2cNumber == I2C0)
	{
		Chip_SCU_I2C0PinConfig( I2C0_STANDARD_FAST_MODE ); // Configuracion de las lineas de SDA y SCL de la placa
	}
	else
	{
		return FALSE;
	}

	Chip_I2C_Init(i2cNumber); //Init I2C0 with default parameters

	Chip_I2C_SetClockRate( i2cNumber, clockRateHz ); // Seleccion de velocidad del bus

	Chip_I2C_EventHandler(I2C0, I2C_EVENT_DONE);

	Chip_I2C_SlaveSetup(i2cNumber, sid, xfer, Chip_I2C_EventHandler, addrMask); //Slave addr set

	return TRUE;
}
