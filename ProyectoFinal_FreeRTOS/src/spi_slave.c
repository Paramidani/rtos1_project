/*
 * spi_slave.c
 *
 *  Created on: Apr 18, 2019
 *      Author: mparamidani
 */

/*==================[inclusions]=============================================*/

#include "spi_slave.h"
#include "chip.h"

bool_t spiSlaveInit( spiMap_t spi )
{
   bool_t retVal = TRUE;
   bool_t spi_status = 0;

   if( spi == SPI0 )
   {
		/* Set up clock and power for SSP1 module */
		// Configure SSP SSP1 pins
		Chip_SCU_PinMuxSet(0xf, 4, (SCU_MODE_PULLUP | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS | SCU_MODE_FUNC0)); // CLK0
		Chip_SCU_PinMuxSet(0x1, 3, (SCU_MODE_PULLUP | SCU_MODE_FUNC5)); // MISO1
		Chip_SCU_PinMuxSet(0x1, 4, (SCU_MODE_PULLUP | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS | SCU_MODE_FUNC5)); // MOSI1
		Chip_SCU_PinMuxSet(0x1, 5, (SCU_MODE_PULLUP | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS | SCU_MODE_FUNC5)); //SSEL

		// Initialize SSP Peripheral
		Chip_SSP_Init( LPC_SSP1 );

		Chip_SSP_SetClockRate(LPC_SSP1, 1, 2);

		Chip_SSP_Set_Mode(LPC_SSP1, SSP_MODE_SLAVE); //same as  Chip_SSP_SetMaster(LPC_SSP1, 0);
		//Chip_SSP_Enable( LPC_SSP1 );
   }
   else {
      retVal = FALSE;
   }

   return retVal;
}

//Blocking function, not used in interrupt mode
bool_t spiSlaveRead( spiMap_t spi, uint8_t* buffer, uint32_t bufferSize )
{
	bool_t retVal = TRUE;

	Chip_SSP_DATA_SETUP_T xferConfig;

	xferConfig.tx_data = NULL;
	xferConfig.tx_cnt  = 0;
	xferConfig.rx_data = buffer;
	xferConfig.rx_cnt  = 0;
	xferConfig.length  = bufferSize;

	if( spi == SPI0 )
	{
		Chip_SSP_RWFrames_Blocking( LPC_SSP1, &xferConfig );
	}
	else
	{
		retVal = FALSE;
	}

	return retVal;
}

//Blocking function, not used in interrupt mode
bool_t spiSlaveWrite( spiMap_t spi, uint8_t* buffer, uint32_t bufferSize)
{
	bool_t retVal = TRUE;

	Chip_SSP_DATA_SETUP_T xferConfig;

	xferConfig.tx_data = buffer;
	xferConfig.tx_cnt  = 0;
	xferConfig.rx_data = NULL;
	xferConfig.rx_cnt  = 0;
	xferConfig.length  = bufferSize;

	if( spi == SPI0 )
	{
		Chip_SSP_RWFrames_Blocking( LPC_SSP1, &xferConfig );
	}
	else
	{
		retVal = FALSE;
	}

	return retVal;
}
