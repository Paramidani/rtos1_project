/*
 * main.h
 *
 *  Created on: Apr 4, 2019
 *      Author: mparamidani
 */

#include "main.h"

SemaphoreHandle_t xSemaphoreSPI = NULL;
SemaphoreHandle_t xSemaphoreI2C = NULL;
SemaphoreHandle_t xSemaphoreUartTx = NULL;
QueueHandle_t rxQueue, txQueue, spiQueue, i2cQueue;

/*
 * variables globales usadas unicamente dentro del main para configurar el periferico i2c (antes de
 * habilitar la interrupción e inciar el scheduler) y dentro del handler de la interrupción de i2c
*/
static I2C_XFER_T i2c_xfer;
uint8_t i2c_TxBuffer[I2C_TX_SIZE];
uint8_t i2c_RxBuffer[I2C_RX_SIZE];

/*
 * void taskUartListen( void* param )
 * --Descripción: Tarea que recibe mensajes (1 byte) desde la uart y luego de
 * recibir 4 bytes retransmite la palabra completa. Unicamente creada para
 * testear el funcionamiento de la uart
 * --Parámetros: no recibe
 * --Despertada por: Cola manejada por el handler de interrupción de la uart
 */
void taskUartListen( void* param )
{
	uint8_t dato, cont = 0;
	txMessageStruct_t txQueueData;
	gpioWrite( LED1, ON );

	txQueueData.txProtocol = 0x75; //letra "u" de uart

	while(1)
	{
			if(xQueueReceive(rxQueue,&dato,portMAX_DELAY) == pdTRUE) //Se recibe un dato desde la interrupción a través de la cola
			{
				txQueueData.txData[cont] = dato; // se almacena el byte recibido
				cont++;
				if(cont == 4) //Cuando se reciben cuatro datos se retransmiten los datos por uart
				{
					Chip_UART_IntDisable(LPC_USART2, (UART_IER_RBRINT | UART_IER_RLSINT));//Se desabilitan la interrupciones de Rx  de la uart
					if(xQueueSend(txQueue,&txQueueData,0) != pdPASS)//Se envía la palabra recibida (4 bytes) a la cola
					{
						printf("ERROR - taskUartListen: fail on writing to txQueue\r\n");
					}
					cont = 0;
					Chip_UART_IntEnable(LPC_USART2, UART_IER_THREINT); //Se habilitan las interrupciones de Tx de la uart para que comience la transmisión del nuevo dato
					Chip_UART_IntEnable(LPC_USART2, (UART_IER_RBRINT | UART_IER_RLSINT)); //Se habilitan las interrupciones de Rx de la uart
				}
			}
			else
			{
				printf("ERROR - taskUartListen: fail on receiving from rxQueue\r\n");
			}
	}
	vTaskDelete( NULL ); //Nunca debería llegar a este punto, si llega se elimina la tarea
}

/*
 * void taskI2CDriver( void* param )
 * --Descripción: tarea que recibe datos una palabra de 3 bytes desde el habndler de interrupción
 * I2C0 y lo transmite por uart
 * --Parámetros: no recibe
 * --Despertada por: Cola manejada por el handler de interrupción i2c0
 */

void taskI2CDriver( void* param )
{
	txMessageStruct_t txQueueDataI2C;
	uint8_t data[3];
	vTaskDelay( 250 / portTICK_RATE_MS ); //Delay inicial unicamente para visualizar como se inicializan las tareas a través de los leds
	gpioWrite( LEDB, ON );
	while(1)
	{
		if(xQueueReceive(i2cQueue,&data,portMAX_DELAY) == pdTRUE) //Espera a recibir un mensaje desde el handler de interrupción
		{
			txQueueDataI2C.txProtocol = 0x69; //Letra "s" de spi
			txQueueDataI2C.txData[0]= data[0]; //Register address recibido
			txQueueDataI2C.txData[1]= data[1]; //Byte 1 del dato recibido
			txQueueDataI2C.txData[2]= data[0]; //Register address recibido
			txQueueDataI2C.txData[3]= data[2]; //Byte 2 del dato recibido
			if(xQueueSend(txQueue,&txQueueDataI2C,0) != pdPASS) //Envía el mensaje recibido hacia la uart
			{
				printf("Error - taskI2CDriver: Fail on writing to txQueue \r\n");
			}
			Chip_UART_IntEnable(LPC_USART2, UART_IER_THREINT);
		}
		else
		{
			printf("ERROR - taskI2CDriver: fail on receiving from i2cQueue\r\n");
		}
	}
	vTaskDelete( NULL ); //Nunca debería llegar a este punto, si llega se elimina la tarea
}

/*
 * void taskSPIDriver( void* param )
 * --Descripción: tarea que recibe datos una palabra de 4 bytes desde el handler de interrupción
 * SSP0 y lo transmite por uart
 * --Parámetros: no recibe
 * --Despertada por: Cola manejada por el handler de interrupción SSP0
 */

void taskSPIDriver( void* param )
{
	txMessageStruct_t txQueueDataSpi;
	uint16_t data[BUFFER_SIZE_SPI/2];
	vTaskDelay( 500 / portTICK_RATE_MS ); //Delay inicial unicamente para visualizar como se inicializan las tareas a través de los leds
	gpioWrite( LED3, ON );
	while(1)
	{
		if(xQueueReceive(spiQueue,&data,portMAX_DELAY) == pdTRUE)
		{
			txQueueDataSpi.txProtocol = 0x73; //letra "s" de spi
			txQueueDataSpi.txData[1]= (uint8_t)(data[0] & 0x00FF);
			txQueueDataSpi.txData[0]= (uint8_t)(data[0]>>8);
			txQueueDataSpi.txData[3]= (uint8_t)(data[1] & 0x00FF);
			txQueueDataSpi.txData[2]= (uint8_t)(data[1]>>8);
			if(xQueueSend(txQueue,&txQueueDataSpi,0) != pdPASS)
			{
				printf("ERROR - taskSPIDriver: Fail on writing to txQueue\r\n");
			}
			Chip_UART_IntEnable(LPC_USART2, UART_IER_THREINT);
			Chip_SSP_Int_Enable(LPC_SSP1);
		}
		else
		{
			printf("ERROR - taskSPIDriver: fail on receiving from spiQueue\r\n");
		}
	}
	vTaskDelete( NULL ); //Nunca debería llegar a este punto, si llega se elimina la tarea
}

/*
 * void taskUartWriter( void* param )
 * --Descripción: tarea que recibe datos desde las otras tareas y los retransmite por la uart
 * --Parámetros: no recibe
 * --Despertada por: Cola manejada txQueue manejada por el resto de las tareas
 */

void taskUartWriter( void* param )
{
	uint8_t dato, cont = 0;
	txMessageStruct_t txQueueData;
	vTaskDelay( 1000 / portTICK_RATE_MS ); //Delay inicial unicamente para visualizar como se inicializan las tareas a través de los leds
	gpioWrite( LED2, ON );
	while(1)
	{
		if(xQueueReceive(txQueue,&txQueueData,portMAX_DELAY) == pdTRUE) //Espera a recibir un mensaje desde alguna de las otras tareas
		{
			if(xSemaphoreTake( xSemaphoreUartTx, portMAX_DELAY ) == pdFALSE) //Espera a que el handler de interrupción libere el semáforo, indicando que la uart esta lista para transmitir
			{
				printf("ERROR - taskUartWriter: fail on taking xSemaphoreUartTx\r\n");
			}
			uartTxWrite( UART_USB, txQueueData.txProtocol ); //Envia el primer byte
			Chip_UART_IntEnable(LPC_USART2, UART_IER_THREINT); //Habilita la interrupcion de uart Tx, previamente deshabilitada por el propio handler
			if(xSemaphoreTake( xSemaphoreUartTx, portMAX_DELAY ) == pdFALSE) //Espera a que el handler de interrupción libere el semáforo, indicando que la uart esta lista para transmitir
			{
				printf("ERROR - taskUartWriter: fail on taking xSemaphoreUartTx\r\n");
			}
			uartTxWrite( UART_USB, txQueueData.txData[0] );
			Chip_UART_IntEnable(LPC_USART2, UART_IER_THREINT);
			if(xSemaphoreTake( xSemaphoreUartTx, portMAX_DELAY ) == pdFALSE) //Espera a que el handler de interrupción libere el semáforo, indicando que la uart esta lista para transmitir
			{
				printf("ERROR - taskUartWriter: fail on taking xSemaphoreUartTx\r\n");
			}
			uartTxWrite( UART_USB, txQueueData.txData[1] );
			Chip_UART_IntEnable(LPC_USART2, UART_IER_THREINT);
			if(xSemaphoreTake( xSemaphoreUartTx, portMAX_DELAY ) == pdFALSE) //Espera a que el handler de interrupción libere el semáforo, indicando que la uart esta lista para transmitir
			{
				printf("ERROR - taskUartWriter: fail on taking xSemaphoreUartTx\r\n");
			}
			uartTxWrite( UART_USB, txQueueData.txData[2] );
			Chip_UART_IntEnable(LPC_USART2, UART_IER_THREINT);
			if(xSemaphoreTake( xSemaphoreUartTx, portMAX_DELAY ) == pdFALSE) //Espera a que el handler de interrupción libere el semáforo, indicando que la uart esta lista para transmitir
			{
				printf("ERROR - taskUartWriter: fail on taking xSemaphoreUartTx\r\n");
			}
			uartTxWrite( UART_USB, txQueueData.txData[3] );
			Chip_UART_IntEnable(LPC_USART2, UART_IER_THREINT);
		}
		else
		{
			printf("ERROR - taskUartWriter: fail on receiving from txQueue\r\n");
		}
	}
	vTaskDelete( NULL ); //Nunca debería llegar a este punto, si llega se elimina la tarea
}

/* void UART2_IRQHandler(void)
 * -- Descripción: handler de la interrupción de la UART2
 */
void UART2_IRQHandler(void)
{
	portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
	uint8_t statusLSR = Chip_UART_ReadLineStatus( LPC_USART2 ); //Se lee el estado de los flags de interrupción
	uint8_t dato;

   // Tx Interrupt
   if( ( statusLSR & UART_LSR_THRE ) && // uartTxReady
	   ( Chip_UART_GetIntsEnabled( LPC_USART2 ) & UART_IER_THREINT ) ) //Interrupción habilitada
   {
	   Chip_UART_IntDisable(LPC_USART2, UART_IER_THREINT); //Se desabilita la interrupción para que no salte de nuevo
	   if(xSemaphoreGiveFromISR( xSemaphoreUartTx, &xHigherPriorityTaskWoken ) != pdTRUE) //Se libera el semáforo para que taskUartWriter pueda enviar un byte
	   {
		   //printf("ERROR - UART2_IRQHandler: fail on giving xSemaphoreUartTx\r\n");
	   }
   }

	// Rx Interrupt
	if(statusLSR & UART_LSR_RDR) // uartRxReady
	{
		dato = uartRxRead(UART_USB); //Se lee el byte recibido, esta operación limpia el flag de la interrupción
		if(xQueueSendFromISR(rxQueue,&dato,&xHigherPriorityTaskWoken) != pdTRUE) //Se envía el rato recibido a la cola despertando a taskUartWriter
		{
			printf("ERROR - UART2_IRQHandler: fail on sending message to rxQueue\r\n");
		}
	}

   portYIELD_FROM_ISR(xHigherPriorityTaskWoken); // Retorna algo?

}

/* void SSP1_IRQHandler(void)
 * -- Descripción: handler de la interrupción SSP1
 */
void SSP1_IRQHandler(void)
{
	portBASE_TYPE xHigherPriorTaskWoken = pdFALSE;
	uint16_t data[BUFFER_SIZE_SPI/2];

	static uint16_t Tx_Buf[BUFFER_SIZE_SPI]; //SPI Tx buffer
	static uint16_t Rx_Buf[BUFFER_SIZE_SPI]; // SPI Rx buffer
	static Chip_SSP_DATA_SETUP_T xf_setup = {Tx_Buf,0,Rx_Buf,0,BUFFER_SIZE_SPI};

	Chip_SSP_Int_Disable(LPC_SSP1);	// Desabilita las interrupciones

	Chip_SSP_Int_RWFrames16Bits(LPC_SSP1, &xf_setup); //Lee el frame recibido en palabras de a 16 bits

	if ((xf_setup.rx_cnt < xf_setup.length) || (xf_setup.tx_cnt < xf_setup.length)) //Si no completo el frame mantiene la interrupcion habilitada
	{
		Chip_SSP_Int_Enable(LPC_SSP1);	// Habilita las interrupciones
	}
	else {
		xf_setup.rx_cnt = 0; // Reinicia el contador del buffer de recepción
		xf_setup.tx_cnt = 0; // Reinicia el contador del buffer de transmisión
		data[0] = *(uint16_t *) ((uint32_t) xf_setup.rx_data); //guarda los datos recibidos
		data[1] = *(uint16_t *) ((uint32_t) xf_setup.rx_data + 2);
		if(xQueueSendFromISR(spiQueue,&data,&xHigherPriorTaskWoken) != pdTRUE) //Envía los datos recibidos por cola a la tarea taskSpiDriver
		{
			printf("ERROR - SSP1_IRQHandler: fail on sending message to spiQueue\r\n");
		}
	}
	portYIELD_FROM_ISR(xHigherPriorTaskWoken); //Retorna algo??
}

/* void I2C0_IRQHandler(void)
 * -- Descripción: handler de la interrupción I2C0
 */
void I2C0_IRQHandler(void)
{
	static uint8_t cont = 0;
	uint8_t data[3];
	portBASE_TYPE xHigherPriorTaskWoken = pdFALSE;

	Chip_I2C_SlaveStateHandler(I2C0); //Lanza la maquina de estados del i2c en modo slave

	if(cont == 4) //Espera a recibir 4 bytes (1 de addres y comando, 3 de datos)
	{
		cont = 0;
		i2c_xfer.txBuff = i2c_TxBuffer; //Reinicia la posición del puntero de transmisión
		i2c_xfer.txSz = I2C_TX_SIZE;
		i2c_xfer.rxBuff = i2c_RxBuffer; //Reinicia la posición del puntero de recepción
		i2c_xfer.rxSz = I2C_RX_SIZE;
		data[0] = *(i2c_xfer.rxBuff); //Guarda los datos recibidos
		data[1] = *(i2c_xfer.rxBuff + 1);
		data[2] = *(i2c_xfer.rxBuff + 2);
		if(xQueueSendFromISR(i2cQueue,&data,&xHigherPriorTaskWoken) != pdTRUE) //Envía los datos recibidos por cola despertando a la tarea taskI2CDriver
		{
			printf("ERROR - I2C0_IRQHandler: fail on sending message to i2cQueue\r\n");
		}
	}
	else
	{
		cont++;
	}
	portYIELD_FROM_ISR(xHigherPriorTaskWoken); //Retorna algo??
}


int main(void)
{
	bool_t spi_status = 0;
	uint8_t dataAux[4];

	static Chip_SSP_DATA_SETUP_T xf_setup;
	static uint16_t Tx_Buf[BUFFER_SIZE_SPI]; //SPI Tx buffer
	static uint16_t Rx_Buf[BUFFER_SIZE_SPI]; // SPI Rx buffer

	boardConfig(); // Inicializa y configura la plataforma

	uartConfig( UART_USB, 115200 ); // Inicializa UART_USB a 115200 baudios

	spiSlaveInit(SPI0); // Inicializa SPI0 en modo slave

	//Creación de semáforos
	xSemaphoreSPI = xSemaphoreCreateBinary();
	xSemaphoreGive( xSemaphoreSPI );
	xSemaphoreTake( xSemaphoreSPI, ( TickType_t ) 10 );

	xSemaphoreI2C = xSemaphoreCreateBinary();
	xSemaphoreGive( xSemaphoreI2C );
	xSemaphoreTake( xSemaphoreI2C, ( TickType_t ) 10 );

	xSemaphoreUartTx = xSemaphoreCreateBinary();
	xSemaphoreGive( xSemaphoreUartTx );
	xSemaphoreTake( xSemaphoreUartTx, ( TickType_t ) 10 );

	//Creación de colas
	rxQueue = xQueueCreate(5, sizeof(uint8_t));
	txQueue = xQueueCreate(5, sizeof(txMessageStruct_t));
	spiQueue = xQueueCreate(5, 2*sizeof(uint16_t));
	i2cQueue = xQueueCreate(5, 3*sizeof(uint8_t));

	gpioWrite( LEDG, ON ); // Led para dar señal de vida

	// Creación de tareas
	xTaskCreate(
		taskUartListen,             	// Funcion de la tarea a ejecutar
		(const char *)"taskUartListen",	// Nombre de la tarea como String amigable para el usuario
		configMINIMAL_STACK_SIZE*2,		// Cantidad de stack de la tarea
		NULL,                       	// Parametros de tarea
		tskIDLE_PRIORITY+1,         	// Prioridad de la tarea
		0			               		// Puntero a la tarea creada en el sistema
	);
	xTaskCreate(taskUartWriter, (const char *)"taskUartWriter", configMINIMAL_STACK_SIZE*2, NULL, tskIDLE_PRIORITY+1, 0);
	xTaskCreate(taskSPIDriver, (const char *)"taskSPIDriver", configMINIMAL_STACK_SIZE*2, NULL, tskIDLE_PRIORITY+1, 0);
	xTaskCreate(taskI2CDriver, (const char *)"taskI2CDriver", configMINIMAL_STACK_SIZE*2, NULL, tskIDLE_PRIORITY+1, 0);

	Chip_UART_IntEnable(LPC_USART2, (UART_IER_RBRINT | UART_IER_RLSINT)); //Se habilitan las interrupciones de Rx de la uart

	NVIC_SetPriority( USART2_IRQn, 5 ); // Se seta la prioridad para la interrupción: FreeRTOS Requiere prioridad >= 5 (numero mas alto, mas baja prioridad)
	NVIC_EnableIRQ(USART2_IRQn); //Se habilita la interrupción de la USART2

	NVIC_SetPriority( SSP1_IRQn, 5 ); // Se seta la prioridad para la interrupción: FreeRTOS Requiere prioridad >= 5 (numero mas alto, mas baja prioridad)
	NVIC_EnableIRQ(SSP1_IRQn); //Se habilita la interrupción de la SSP1

	//Estructura de inicialización del módulo SPI
	xf_setup.length = BUFFER_SIZE_SPI;
	xf_setup.tx_data = Tx_Buf;
	xf_setup.rx_data = Rx_Buf;
	xf_setup.rx_cnt = 0;
	xf_setup.tx_cnt = 0;

	Chip_SSP_Int_FlushData(LPC_SSP1); //Se vacía el buffer del SPI de datos "basura"
	Chip_SSP_Int_RWFrames8Bits(LPC_SSP1, &xf_setup); //Se reliza una primera lectura, necesaria para la inicalización
	Chip_SSP_Int_Enable(LPC_SSP1); //Se habilita la interrupción SSP1 desde el registro de configuración del periférico

	//Estructura de inicialización del módulo I2C
	i2c_xfer.slaveAddr = (SLAVE_ADDR << 1);
	i2c_xfer.txBuff = i2c_TxBuffer;
	i2c_xfer.txSz = I2C_TX_SIZE;
	i2c_xfer.rxBuff = i2c_RxBuffer;
	i2c_xfer.rxSz = I2C_RX_SIZE;

	i2cSlaveInit( I2C0, &i2c_xfer, 0 ); //Inicialización del módulo I2C en modo slave

	NVIC_SetPriority( I2C0_IRQn, 5 ); // Se seta la prioridad para la interrupción: FreeRTOS Requiere prioridad >= 5 (numero mas alto, mas baja prioridad)
	NVIC_EnableIRQ(I2C0_IRQn); //Se habilita la interrupción del I2C


	printf("Everything ready, Program started\r\n");

	vTaskStartScheduler(); // Se inicia el scheduler

	while( TRUE ) {
	}

	return 0;
}
