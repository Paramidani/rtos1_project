/*
 * spi_slave.h
 *
 *  Created on: Apr 18, 2019
 *      Author: mparamidani
 */

#ifndef _SPI_SLAVE_H_
#define _SPI_SLAVE_H_

/*==================[inclusions]=============================================*/

#include "sapi_datatypes.h"
#include "sapi_peripheral_map.h"

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*==================[macros]=================================================*/

#define spiSlaveConfig spiSlaveInit

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[ISR external functions definition]======================*/

/*==================[external functions definition]==========================*/

bool_t spiSlaveInit( spiMap_t spi );

bool_t spiSlaveRead( spiMap_t spi, uint8_t* buffer, uint32_t bufferSize );

bool_t spiSlaveWrite( spiMap_t spi, uint8_t* buffer, uint32_t bufferSize);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[end of file]============================================*/
#endif /* _SPI_SLAVE_H_ */
