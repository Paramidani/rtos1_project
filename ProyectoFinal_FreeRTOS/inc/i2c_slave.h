/*
 * i2c_slave.h
 *
 *  Created on: Apr 24, 2019
 *      Author: mparamidani
 */

#ifndef _I2C_SLAVE_H_
#define _I2C_SLAVE_H_

#include "sapi_datatypes.h"
#include "sapi_peripheral_map.h"

#define SLAVE_ADDR 0b1010101 //en hexa seria 0x55
#define I2C_CLK_RATE 400000
#define I2C_TX_SIZE 4
#define I2C_RX_SIZE 4

//static void i2c_slave_int_events(I2C_ID_T id, I2C_EVENT_T event);

bool_t i2cSlaveInit( i2cMap_t i2cNumber, I2C_XFER_T *xfer, uint8_t addrMask );


#endif /* _I2C_SLAVE_H_ */
