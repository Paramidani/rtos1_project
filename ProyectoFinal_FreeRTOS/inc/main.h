/*
 * main.h
 *
 *  Created on: Apr 4, 2019
 *      Author: mparamidani
 */

#ifndef _MAIN_H_
#define _MAIN_H_

/*==================[inlcusiones]============================================*/

// Includes de FreeRTOS
#include "FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"

// sAPI header
#include "sapi.h"

// My headers
#include "spi_slave.h"
#include "i2c_slave.h"

#define I2C_FREQ 400000
#define BUFFER_SIZE_SPI 4

void taskUartListen( void* param );
void taskI2CDriver( void* param );
void taskSPIDriver( void* param );
void taskUartWriter( void* param );
void UART0_IRQHandler(void);
//void SSP1_IRQHandler(void);

typedef struct{
	uint8_t rxProtocol;
	uint8_t rxData[4];
}rxMessageStruct_t;

typedef struct{
	uint8_t txProtocol;
	uint8_t txData[4];
}txMessageStruct_t;



#endif /* _MAIN_H_ */
