/* Copyright 2017-2018, Eric Pernia
 * All rights reserved.
 *
 * This file is part of sAPI Library.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*==================[inlcusiones]============================================*/

// Includes de FreeRTOS
#include "../../C2_E3_FreeRTOS/inc/FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include "task.h"

// sAPI header
#include "sapi.h"

#define TIEMPO_DEBOUNCE 400 //Tiempo de 4seg para que se aprecie el debounce, para 40ms usar 40

/*==================[definiciones y macros]==================================*/

/*==================[definiciones de datos internos]=========================*/

/*==================[definiciones de datos externos]=========================*/

DEBUG_PRINT_ENABLE;

/*==================[declaraciones de funciones internas]====================*/

/*==================[declaraciones de funciones externas]====================*/

// Prototipo de funcion de la tarea
void myTaskLed( void* taskParmPtr );
void myTaskDebounce( void* taskParmPtr );

portTickType tiempoPresionado;
TaskHandle_t xHandle;

/*==================[funcion principal]======================================*/

// FUNCION PRINCIPAL, PUNTO DE ENTRADA AL PROGRAMA LUEGO DE ENCENDIDO O RESET.
int main(void)
{
   // ---------- CONFIGURACIONES ------------------------------
   // Inicializar y configurar la plataforma
   boardConfig();

   // UART for debug messages
   debugPrintConfigUart( UART_USB, 115200 );
   debugPrintlnString( "Blinky con freeRTOS y sAPI." );

   // Led para dar se�al de vida
   gpioWrite( LED3, ON );

   // Crear tarea en freeRTOS
   xTaskCreate(
      myTaskLed,                     // Funcion de la tarea a ejecutar
      (const char *)"myTaskLed",     // Nombre de la tarea como String amigable para el usuario
      configMINIMAL_STACK_SIZE*2, // Cantidad de stack de la tarea
      0,                          // Parametros de tarea
      tskIDLE_PRIORITY+1,         // Prioridad de la tarea
      &xHandle                           // Puntero a la tarea creada en el sistema
   );

   // Crear tarea en freeRTOS
   xTaskCreate(
      myTaskDebounce,                     // Funcion de la tarea a ejecutar
      (const char *)"myTaskDebounce",     // Nombre de la tarea como String amigable para el usuario
      configMINIMAL_STACK_SIZE*2, // Cantidad de stack de la tarea
      0,                          // Parametros de tarea
      tskIDLE_PRIORITY+1,         // Prioridad de la tarea
      0                           // Puntero a la tarea creada en el sistema
   );

   // Iniciar scheduler
   vTaskStartScheduler();

   // ---------- REPETIR POR SIEMPRE --------------------------
   while( TRUE ) {
      // Si cae en este while 1 significa que no pudo iniciar el scheduler
   }

   // NO DEBE LLEGAR NUNCA AQUI, debido a que a este programa se ejecuta
   // directamenteno sobre un microcontroladore y no es llamado por ningun
   // Sistema Operativo, como en el caso de un programa para PC.
   return 0;
}

/*==================[definiciones de funciones internas]=====================*/

/*==================[definiciones de funciones externas]=====================*/

// Implementacion de funcion de la tarea
void myTaskLed( void* taskParmPtr )
{
   gpioWrite( LED1, ON );
   // Envia la tarea al estado bloqueado durante 1 s (delay)
   vTaskDelay( 1000 / portTICK_RATE_MS );
   gpioWrite( LED1, OFF );

   while(1)
   {
	   vTaskSuspend( xHandle );
	   gpioWrite( LEDB, ON );
	   printf( "Encendido:)\r\n" );
	   vTaskDelay(tiempoPresionado);
	   gpioWrite( LEDB, OFF );
	   printf( "Apagado:(\r\n" );

   }
}

void myTaskDebounce( void* taskParmPtr )
{

   // Nuevo tipo de datos enumerado llamado estadoMEF
  typedef enum{
		   BUTTON_UP,
		   BUTTON_FALLING,
		   BUTTON_DOWN,
		   BUTTON_RAISING
  } estadoMEF;

  // Variable de estado
  estadoMEF  estadoActual;
  estadoActual=BUTTON_UP;

  /* Variable de Retardo no bloqueante */
  delay_t delay_falling;
  delay_t delay_raising;

   gpioWrite( LED2, ON );
   // Envia la tarea al estado bloqueado durante 1 s (delay)
   vTaskDelay( 1000 / portTICK_RATE_MS );
   gpioWrite( LED2, OFF );

   portTickType xTimePressed = 0;
   portTickType xTimeReleased = 0;

   
   // ---------- REPETIR POR SIEMPRE --------------------------
   while(TRUE) {
	   //vTaskDelay( 500 / portTICK_RATE_MS );
	   switch (estadoActual) {

	   	   		case BUTTON_UP:
	   	   		{
	   	   			if(!gpioRead( TEC1 ))
	   	   			{
	   	   				estadoActual = BUTTON_FALLING;
	   	   				vTaskDelay( TIEMPO_DEBOUNCE / portTICK_RATE_MS );
	   	   			    //delayConfig( &delay_falling, TIEMPO_DEBOUNCE );

	   	   			}
	   	   			else
	   	   			{
	   	   				vTaskDelay( 500 / portTICK_RATE_MS );
	   	   			}
	   	   		}
	   	   		break;

	   	   		case BUTTON_FALLING:
	   	   		{

					if(!gpioRead( TEC1 ))
					{
						estadoActual = BUTTON_DOWN;
						//buttonPressed();
						xTimePressed = xTaskGetTickCount();

					}
					else
					{
						estadoActual = BUTTON_UP;
					}
	   	   		}
	   	   		break;

	   	   		case BUTTON_DOWN:
	   	   		{
	   	   			if(gpioRead( TEC1 ))
	   	   			{
	   	   				estadoActual = BUTTON_RAISING;
	   	   				vTaskDelay( TIEMPO_DEBOUNCE / portTICK_RATE_MS );
	   	   			    //delayConfig( &delay_raising, TIEMPO_DEBOUNCE );
	   	   			}
	   	   			else
					{
						vTaskDelay( 500 / portTICK_RATE_MS );
					}
	   	   		}
	   	   		break;

	   	   		case BUTTON_RAISING:
	   	   		{
					if(gpioRead( TEC1 ))
					{
						estadoActual = BUTTON_UP;
						//buttonReleased();
						xTimeReleased = xTaskGetTickCount();
						tiempoPresionado = xTimeReleased-xTimePressed;
						vTaskResume(xHandle);

					}
					else
					{
						estadoActual = BUTTON_DOWN;
					}
	   	   		}
	   	   		break;

	   	   		default:
	   	   		{
	   	   			estadoActual = BUTTON_UP;
	   	   			vTaskDelay( 500 / portTICK_RATE_MS );
	   	   		}
	   	   		break;

	   	   }




/*
      gpioWrite( LEDB, ON );
      printf( "Encendido:)\r\n" );
      vTaskDelay(xTimeOn);
      gpioWrite( LEDB, OFF );
      printf( "Apagado:(\r\n" );
      // Envia la tarea al estado bloqueado durante xPeriodicity (delay periodico)
      vTaskDelayUntil( &xLastWakeTime, xPeriodicity );
      if(xTimeOn==900/portTICK_RATE_MS)
      {
    	  xTimeOn = 100/portTICK_RATE_MS;
      }
      else
      {
    	  xTimeOn = xTimeOn + 100/portTICK_RATE_MS;
      }
*/
   }
}

/*==================[fin del archivo]========================================*/
