/* Copyright 2017-2018, Eric Pernia
 * All rights reserved.
 *
 * This file is part of sAPI Library.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*==================[inlcusiones]============================================*/

// Includes de FreeRTOS
#include "../../C2_E3_FreeRTOS/inc/FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include "task.h"

// sAPI header
#include "sapi.h"

#define TIEMPO_DEBOUNCE 400 //Tiempo de 4seg para que se aprecie el debounce, para 40ms usar 40

/*==================[definiciones y macros]==================================*/

/*==================[definiciones de datos internos]=========================*/

portTickType tiempoPresionado1, tiempoPresionado2, tiempoPresionado3;
TaskHandle_t xHandle1, xHandle2, xHandle3;

typedef struct{
	gpioMap_t led_out;
	TaskHandle_t xHandle;
	portTickType *tiempoPresionado;
} blinkTaskStruct;

blinkTaskStruct led_out_1, led_out_2, led_out_3;

typedef struct{
	gpioMap_t key;
	TaskHandle_t xHandle;
	portTickType *tiempoPresionado;
} debounceTaskStruct;

debounceTaskStruct key_1, key_2, key_3;


/*==================[definiciones de datos externos]=========================*/

DEBUG_PRINT_ENABLE;

/*==================[declaraciones de funciones internas]====================*/

// Prototipo de funcion de la tarea
void myTaskLed( void* param );//blinkTaskStruct* param );
void myTaskDebounce( void* param );//debounceTaskStruct* param );

/*==================[declaraciones de funciones externas]====================*/


/*==================[funcion principal]======================================*/

// FUNCION PRINCIPAL, PUNTO DE ENTRADA AL PROGRAMA LUEGO DE ENCENDIDO O RESET.
int main(void)
{
   // ---------- CONFIGURACIONES ------------------------------
   // Inicializar y configurar la plataforma
   boardConfig();

   // UART for debug messages
   debugPrintConfigUart( UART_USB, 115200 );
   debugPrintlnString( "Blinky con freeRTOS y sAPI." );

   // Led para dar se�al de vida
   gpioWrite( LEDG, ON );

   // Crear tarea en freeRTOS
   xTaskCreate(
      myTaskLed,                  // Funcion de la tarea a ejecutar
      (const char *)"myTaskLed1",  // Nombre de la tarea como String amigable para el usuario
      configMINIMAL_STACK_SIZE*2, // Cantidad de stack de la tarea
	  (void*)&led_out_1,                          // Parametros de tarea
      tskIDLE_PRIORITY+1,         // Prioridad de la tarea
      &xHandle1                   // Puntero a la tarea creada en el sistema
   );

   xTaskCreate(
      myTaskLed,                  // Funcion de la tarea a ejecutar
      (const char *)"myTaskLed2",  // Nombre de la tarea como String amigable para el usuario
      configMINIMAL_STACK_SIZE*2, // Cantidad de stack de la tarea
	  (void*)&led_out_2,                          // Parametros de tarea
      tskIDLE_PRIORITY+1,         // Prioridad de la tarea
      &xHandle2                   // Puntero a la tarea creada en el sistema
   );

   xTaskCreate(
      myTaskLed,                  // Funcion de la tarea a ejecutar
      (const char *)"myTaskLed3",  // Nombre de la tarea como String amigable para el usuario
      configMINIMAL_STACK_SIZE*2, // Cantidad de stack de la tarea
	  (void*)&led_out_3,                          // Parametros de tarea
      tskIDLE_PRIORITY+1,         // Prioridad de la tarea
      &xHandle3                   // Puntero a la tarea creada en el sistema
   );


   led_out_1.led_out = LED1;
   led_out_2.led_out = LED2;
   led_out_3.led_out = LED3;
   led_out_1.xHandle = xHandle1;
   led_out_2.xHandle = xHandle2;
   led_out_3.xHandle = xHandle3;
   led_out_1.tiempoPresionado = &tiempoPresionado1;
   led_out_2.tiempoPresionado= &tiempoPresionado2;
   led_out_3.tiempoPresionado = &tiempoPresionado3;

   key_1.key = TEC1;
   key_2.key = TEC2;
   key_3.key = TEC3;
   key_1.xHandle = xHandle1;
   key_2.xHandle = xHandle2;
   key_3.xHandle = xHandle3;
   key_1.tiempoPresionado = &tiempoPresionado1;
   key_2.tiempoPresionado = &tiempoPresionado2;
   key_3.tiempoPresionado = &tiempoPresionado3;

   // Crear tarea en freeRTOS
   xTaskCreate(
      myTaskDebounce,                     // Funcion de la tarea a ejecutar
      (const char *)"myTaskDebounce1",     // Nombre de la tarea como String amigable para el usuario
      configMINIMAL_STACK_SIZE*2, // Cantidad de stack de la tarea
	  (void*)&key_1,                          // Parametros de tarea
      tskIDLE_PRIORITY+1,         // Prioridad de la tarea
      0                           // Puntero a la tarea creada en el sistema
   );

   xTaskCreate(
      myTaskDebounce,                     // Funcion de la tarea a ejecutar
      (const char *)"myTaskDebounce2",     // Nombre de la tarea como String amigable para el usuario
      configMINIMAL_STACK_SIZE*2, // Cantidad de stack de la tarea
	  (void*)&key_2,                          // Parametros de tarea
      tskIDLE_PRIORITY+1,         // Prioridad de la tarea
      0                           // Puntero a la tarea creada en el sistema
   );

   xTaskCreate(
      myTaskDebounce,                     // Funcion de la tarea a ejecutar
      (const char *)"myTaskDebounce3",     // Nombre de la tarea como String amigable para el usuario
      configMINIMAL_STACK_SIZE*2, // Cantidad de stack de la tarea
	  (void*)&key_3,                          // Parametros de tarea
      tskIDLE_PRIORITY+1,         // Prioridad de la tarea
      0                           // Puntero a la tarea creada en el sistema
   );

   // Iniciar scheduler
   vTaskStartScheduler();

   // ---------- REPETIR POR SIEMPRE --------------------------
   while( TRUE ) {
      // Si cae en este while 1 significa que no pudo iniciar el scheduler
   }

   // NO DEBE LLEGAR NUNCA AQUI, debido a que a este programa se ejecuta
   // directamenteno sobre un microcontroladore y no es llamado por ningun
   // Sistema Operativo, como en el caso de un programa para PC.
   return 0;
}

/*==================[definiciones de funciones internas]=====================*/

/*==================[definiciones de funciones externas]=====================*/

// Implementacion de funcion de la tarea
void myTaskLed( void* param )
{
   gpioWrite( ((blinkTaskStruct*)param)->led_out, ON );
   // Envia la tarea al estado bloqueado durante 1 s (delay)
   vTaskDelay( 1000 / portTICK_RATE_MS );
   gpioWrite( ((blinkTaskStruct*)param)->led_out, OFF );

   while(1)
   {
	   vTaskSuspend( ((blinkTaskStruct*)param)->xHandle );
	   gpioWrite( ((blinkTaskStruct*)param)->led_out, ON );
	   printf( "Encendido:)\r\n" );
	   vTaskDelay(*(((blinkTaskStruct*)param)->tiempoPresionado));
	   gpioWrite( ((blinkTaskStruct*)param)->led_out, OFF );
	   printf( "Apagado:(\r\n" );

   }
   vTaskDelete( NULL );
}

void myTaskDebounce( void* param )
{

   // Nuevo tipo de datos enumerado llamado estadoMEF
  typedef enum{
		   BUTTON_UP,
		   BUTTON_FALLING,
		   BUTTON_DOWN,
		   BUTTON_RAISING
  } estadoMEF;

  // Variable de estado
  estadoMEF  estadoActual;
  estadoActual=BUTTON_UP;

   vTaskDelay( 1000 / portTICK_RATE_MS );

   portTickType xTimePressed = 0;
   portTickType xTimeReleased = 0;

   
   // ---------- REPETIR POR SIEMPRE --------------------------
   while(TRUE) {
	   //vTaskDelay( 100 / portTICK_RATE_MS );
	   switch (estadoActual) {

	   	   		case BUTTON_UP:
	   	   		{
	   	   			if(!gpioRead(((debounceTaskStruct*)param)->key))
	   	   			{
	   	   				estadoActual = BUTTON_FALLING;
	   	   				vTaskDelay( TIEMPO_DEBOUNCE / portTICK_RATE_MS );

	   	   			}
	   	   			else
	   	   			{
	   	   				vTaskDelay( 500 / portTICK_RATE_MS );
	   	   			}
	   	   		}
	   	   		break;

	   	   		case BUTTON_FALLING:
	   	   		{

					if(!gpioRead(((debounceTaskStruct*)param)->key))
					{
						estadoActual = BUTTON_DOWN;
						//buttonPressed();
						xTimePressed = xTaskGetTickCount();
						//vTaskDelay( 100 / portTICK_RATE_MS );

					}
					else
					{
						estadoActual = BUTTON_UP;
						//vTaskDelay( 100 / portTICK_RATE_MS );
					}
	   	   		}
	   	   		break;

	   	   		case BUTTON_DOWN:
	   	   		{
	   	   			if(gpioRead(((debounceTaskStruct*)param)->key))
	   	   			{
	   	   				estadoActual = BUTTON_RAISING;
	   	   				vTaskDelay( TIEMPO_DEBOUNCE / portTICK_RATE_MS );
	   	   			}
	   	   			else
					{
						vTaskDelay( 500 / portTICK_RATE_MS );
					}
	   	   		}
	   	   		break;

	   	   		case BUTTON_RAISING:
	   	   		{
					if(gpioRead(((debounceTaskStruct*)param)->key))
					{
						estadoActual = BUTTON_UP;
						//buttonReleased();
						xTimeReleased = xTaskGetTickCount();
						*(((debounceTaskStruct*)param)->tiempoPresionado) = xTimeReleased-xTimePressed;
						vTaskResume(((debounceTaskStruct*)param)->xHandle);
						//vTaskDelay( 100 / portTICK_RATE_MS );

					}
					else
					{
						estadoActual = BUTTON_DOWN;
						//vTaskDelay( 100 / portTICK_RATE_MS );
					}
	   	   		}
	   	   		break;

	   	   		default:
	   	   		{
	   	   			estadoActual = BUTTON_UP;
	   	   			vTaskDelay( 500 / portTICK_RATE_MS );
	   	   		}
	   	   		break;

	   	   }
   }
   vTaskDelete( NULL );
}

/*==================[fin del archivo]========================================*/
